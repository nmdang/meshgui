# MeshViewer #

A simple UI to load and render a 3D mesh using Qt, QGLViewer and OpenMesh. It supports point, wireframe, flat shading and smooth shading. The framework also allows to take a screenshot and detect and visualize connected components.