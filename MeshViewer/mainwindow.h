#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <map>
#include <vector>
#include <QMainWindow>
#include "mesh.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
public slots:
    // Load a mesh from file.
    void loadMesh();
    // Write a mesh to file.
    void exportMesh();

private:
    // Find connected components of the current mesh if exist
    // Label each vertex with the corresponding component ID
    void segment();

private:
    Ui::MainWindow  *ui;
    Mesh            m_mesh;
    // connected components
    std::map<unsigned int, std::vector<unsigned int> >  m_components;
};

#endif // MAINWINDOW_H
