#include "meshutils.h"

using namespace Eigen;

Mesh MeshUtils::extractMesh(const Mesh &input,
                            const std::vector<unsigned int> &vertices) {
    Mesh output = input;
    OpenMesh::VPropHandleT<bool>    selected;
    output.add_property(selected, "selected");

    Mesh::VertexIter vIt(output.vertices_begin()), vEnd(output.vertices_end());

    for (; vIt != vEnd; ++vIt) {
        output.property(selected, vIt) = false;
    }

    for (unsigned int i = 0; i < vertices.size(); i++) {
        Mesh::VertexHandle vh(vertices[i]);
        output.property(selected, vh) = true;
    }

    for (vIt = output.vertices_begin(); vIt != vEnd; ++vIt) {
        if (!output.property(selected, vIt)) {
            output.delete_vertex(vIt);
        }
    }
    output.garbage_collection();
    output.remove_property(selected);
    return output;
}

unsigned int MeshUtils::findConnectedComponent(Mesh &mesh) {
    unsigned int ncomponents = 0;
    OpenMesh::VPropHandleT<unsigned int>    groupId;
    OpenMesh::VPropHandleT<bool>            selected;

    if (!mesh.get_property_handle(selected, "selected")) {
        mesh.add_property(selected, "selected");
    }

    if (!mesh.get_property_handle(groupId, "groupId")) {
        mesh.add_property(groupId, "groupId");
    }

    Mesh::VertexIter vIt, vBegin(mesh.vertices_begin()),
            vEnd(mesh.vertices_end());

    for (vIt = vBegin; vIt != vEnd; ++vIt) {
        mesh.property(selected, vIt) = false;
    }

    for (vIt = vBegin; vIt != vEnd; ++vIt) {
        if (mesh.property(selected, vIt)) continue;
        mesh.property(selected, vIt) = true;
        mesh.property(groupId, vIt) = ncomponents;
        std::vector<Mesh::VertexHandle> vhStack;
        vhStack.push_back(vIt.handle());
        while (!vhStack.empty()) {
            Mesh::VertexHandle vh = vhStack.back();
            vhStack.pop_back();
            Mesh::VertexVertexIter vvIt;
            for (vvIt = mesh.vv_iter(vh); vvIt; ++vvIt) {
                if (mesh.property(selected, vvIt)) continue;
                mesh.property(selected, vvIt) = true;
                mesh.property(groupId, vvIt) = ncomponents;
                vhStack.push_back(vvIt.handle());
            }
        }
        ncomponents++;
    }

    mesh.remove_property(selected);
    return ncomponents;
}

MatrixXf MeshUtils::points2Mat(const Mesh &mesh,
                               std::vector<unsigned int> vertices) {
    unsigned int npoints = vertices.size();
    MatrixXf output = MatrixXf::Zero(3, npoints);
    for (unsigned int i = 0; i < npoints; i++) {
        Mesh::Point point = mesh.point(Mesh::VertexHandle(vertices[i]));
        for (unsigned int j = 0; j < 3; j++) {
            output(j, i) = point[j];
        }
    }
    return output;
}
