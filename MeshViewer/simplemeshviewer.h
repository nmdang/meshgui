#ifndef SIMPLEMESHVIEWER_H
#define SIMPLEMESHVIEWER_H

#include <vector>
#include <QColor>
#include <QGLViewer/qglviewer.h>
#include "mesh.h"

enum ViewMode {
    WIREFRAME = 0,
    FLAT,
    SMOOTH,
    COMPONENT,
    POINT
};

class SimpleMeshViewer : public QGLViewer
{
    Q_OBJECT
public:
    SimpleMeshViewer(QWidget *_parent = 0);
    ~SimpleMeshViewer();

public:
    void init();
    void draw();
    void drawWithNames();
    void postSelection(const QPoint &point);

    const Mesh* mesh() const {return m_mesh;}
    void setMesh(Mesh *mesh);

public slots:
    void setWireFrameMode() {
        m_viewMode = WIREFRAME;
        updateGL();
    }

    void setFlatMode() {
        m_viewMode = FLAT;
        updateGL();
    }

    void setSmoothMode() {
        m_viewMode = SMOOTH;
        updateGL();
    }

    void setComponentMode() {
        m_viewMode = COMPONENT;
        updateGL();
    }

    void setPointMode() {
        m_viewMode = POINT;
        updateGL();
    }

    void takeScreenShot();

private:
    void drawMesh(bool withName = false);
    void drawPoint();

    void renderOffscreen(unsigned int width, unsigned int height,
                         unsigned char *data);

    std::vector<float> getFaceColor(Mesh::FaceHandle fh);

private:
    Mesh*           m_mesh;
    ViewMode        m_viewMode;
    qglviewer::Vec  m_orig, m_dir, m_selectedPoint;

public:
    // Dynamic allocation of random color for each mesh component
    static std::vector<QColor>  componentColorMap;
    QColor  getComponentColor(unsigned int id);
};

#endif // SIMPLEMESHVIEWER_H
