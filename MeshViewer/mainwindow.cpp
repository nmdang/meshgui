#include <QFileDialog>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QDebug>
#include <sstream>
#include <limits>
#include <Eigen/Dense>
#include "ui_mainwindow.h"
#include "simplemeshviewer.h"
#include "meshutils.h"

#include "mainwindow.h"

using namespace Eigen;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadMesh() {
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open mesh file"),
        tr(""),
        tr("OBJ Files (*.obj);;"
        "OFF Files (*.off);;"
        "STL Files (*.stl);;"
        "All Files (*)"));
    if (!fileName.isEmpty()) {
        m_mesh = Mesh();
        m_mesh.request_face_normals();
        m_mesh.request_vertex_normals();
        OpenMesh::IO::Options ropt;
        ropt += OpenMesh::IO::Options::VertexNormal;
        if (OpenMesh::IO::read_mesh(m_mesh, fileName.toLocal8Bit().constData()),
                ropt)
        {
            m_mesh.update_face_normals();
            m_mesh.update_vertex_normals();
            segment();
            static_cast<SimpleMeshViewer*>(centralWidget())->setMesh(&m_mesh);
        }
    }
}

void MainWindow::exportMesh() {
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save mesh file"),
        tr(""),
        tr("OBJ Files (*.obj);;"
        "OFF Files (*.off);;"
        "STL Files (*.stl);;"
        "All Files (*)"));

    OpenMesh::IO::Options wopt;
    wopt += OpenMesh::IO::Options::VertexNormal;

    if (!fileName.isEmpty()) {
        if (!OpenMesh::IO::write_mesh(m_mesh,
                                      fileName.toLocal8Bit().constData(),
                                      wopt))
        {
          std::cerr << "write error\n";
          exit(1);
        }
    }

    qDebug() << "export mesh to " << fileName;
}

void MainWindow::segment() {
    OpenMesh::VPropHandleT<unsigned int>    groupId;

    if (!m_mesh.get_property_handle(groupId, "groupId")) {
        MeshUtils::findConnectedComponent(m_mesh);
        m_mesh.get_property_handle(groupId, "groupId");
    }

    Mesh::VertexIter vIt, vBegin(m_mesh.vertices_begin()),
            vEnd(m_mesh.vertices_end());

    for (vIt = vBegin; vIt != vEnd; ++vIt) {
        unsigned int gid = m_mesh.property(groupId, vIt);
        unsigned int vid = vIt.handle().idx();
        m_components[gid].push_back(vid);
    }
}
