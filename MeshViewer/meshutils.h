#ifndef MESHUTILS_H
#define MESHUTILS_H

#include <vector>
#include <Eigen/Dense>
#include "mesh.h"

class MeshUtils
{
public:
    // Extract a sub-mesh of the input mesh indicated by vertex indices.
    static Mesh extractMesh(const Mesh& input,
                             const std::vector<unsigned int> &vertices);

    // Find connected components of a mesh and label mesh vertices with the associated mesh component ID
    static unsigned int findConnectedComponent(Mesh &mesh);

    // Return vertex position in Eigen::MatrixXf format.
    static Eigen::MatrixXf points2Mat(const Mesh& mesh,
                                      std::vector<unsigned int> vertices);

private:
    MeshUtils(){}
};

#endif // MESHUTILS_H
