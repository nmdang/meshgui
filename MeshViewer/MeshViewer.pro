#-------------------------------------------------
#
# Project created by QtCreator 2013-06-03T14:05:41
#
#-------------------------------------------------

QT       += core gui opengl xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MeshViewer
TEMPLATE = app

LIBS += -framework QGLViewer

LIBS += -L/usr/local/lib/OpenMesh -lOpenMeshCore

SOURCES += main.cpp\
    simplemeshviewer.cpp \
    mainwindow.cpp \
    meshutils.cpp \

HEADERS  += mainwindow.h \
    simplemeshviewer.h \
    meshutils.h \
    mesh.h \

INCLUDEPATH += /usr/local/include/eigen3

FORMS    += mainwindow.ui

