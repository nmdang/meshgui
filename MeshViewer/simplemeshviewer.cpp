#include <cassert>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QImage>

#include <Eigen/Dense>

#include "simplemeshviewer.h"

std::vector<QColor> SimpleMeshViewer::componentColorMap =
        std::vector<QColor>();

QColor SimpleMeshViewer::getComponentColor(unsigned int id) {
    if (id >= componentColorMap.size())
    {
        while (id >= componentColorMap.size())
        {
            int r1 = ((float)rand()/RAND_MAX)*255.0f;
            int r2 = ((float)rand()/RAND_MAX)*255.0f;
            int r3 = ((float)rand()/RAND_MAX)*255.0f;
            QColor randcolor = QColor(r1,r2,r3);
            componentColorMap.push_back(randcolor);
        }
    }
    return componentColorMap.at(id);
}

SimpleMeshViewer::SimpleMeshViewer(QWidget* _parent) :
    QGLViewer(_parent),
    m_mesh(NULL), m_viewMode(COMPONENT)
{
}

SimpleMeshViewer::~SimpleMeshViewer() {
}

void SimpleMeshViewer::init() {
    setBackgroundColor(QColor(255,255,255));
    camera()->setSceneRadius(0.5);
    showEntireScene();
}

void SimpleMeshViewer::draw() {
    switch(m_viewMode) {
    case WIREFRAME:
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        break;
    case FLAT:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glShadeModel(GL_FLAT);
        break;
    case SMOOTH:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glShadeModel(GL_SMOOTH);
        break;
    case COMPONENT:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glShadeModel(GL_SMOOTH);
        break;
    default:
        break;
    }

    if (m_mesh) {
        if (m_viewMode == POINT) {
            drawPoint();
        } else {
            drawMesh(false);
        }
    }
}

void SimpleMeshViewer::drawWithNames() {
    if (m_viewMode == POINT) return;
    if (!m_mesh) return;
    drawMesh(true);
}

void SimpleMeshViewer::postSelection(const QPoint &point) {
    Q_UNUSED(point)
}

void SimpleMeshViewer::takeScreenShot() {
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save Screenshot"),
        tr(""),
        tr("Images (*.png *.xpm *.jpg)"));

    GLint viewportDimensions[4];
    glGetIntegerv(GL_VIEWPORT, viewportDimensions);
    unsigned int imageWidth = viewportDimensions[2];
    unsigned int imageHeight = viewportDimensions[3];
    unsigned char * data = new unsigned char[imageWidth * imageHeight*4];

    renderOffscreen(imageWidth, imageHeight, data);

    unsigned char* imageData = new unsigned char[imageWidth * imageHeight*4];

    unsigned int i = 0;
    for(unsigned int y = 0; y < imageHeight; y++) {
        for(unsigned int x = 0; x < imageWidth; x++, i+= 4) {
            // red
            *(imageData + 4*((imageHeight-y-1) * imageWidth + x) + 2) = data[i];
            // green
            *(imageData + 4*((imageHeight-y-1) * imageWidth + x) + 1) = data[i+1];
            // blue
            *(imageData + 4*((imageHeight-y-1) * imageWidth + x) + 0) = data[i+2];
            // alpha
            *(imageData + 4*((imageHeight-y-1) * imageWidth + x) + 3) = data[i+3];
        }
    }

    QImage img(imageData, imageWidth, imageHeight, QImage::Format_ARGB32);
//    img.invertPixels();

    img.save(fileName, 0, -1);
    delete [] imageData;
    delete [] data;
}

void SimpleMeshViewer::setMesh(Mesh *mesh) {
    m_mesh = mesh;

    // bounding box
    Mesh::ConstVertexIter vIt(m_mesh->vertices_begin());
    Mesh::ConstVertexIter vEnd(m_mesh->vertices_end());

    typedef Mesh::Point Point;
    using OpenMesh::Vec3f;

    Vec3f bbMin, bbMax;

    bbMin = bbMax = OpenMesh::vector_cast<Vec3f>(m_mesh->point(vIt));

    for (size_t count=0; vIt!=vEnd; ++vIt, ++count)
    {
     bbMin.minimize( OpenMesh::vector_cast<Vec3f>(m_mesh->point(vIt)));
     bbMax.maximize( OpenMesh::vector_cast<Vec3f>(m_mesh->point(vIt)));
    }

    setSceneCenter(0.5 * qglviewer::Vec(bbMin[0] + bbMax[0],
           bbMin[1] + bbMax[1], bbMin[2] + bbMax[2]));
    camera()->setSceneRadius((bbMin-bbMax).norm()*0.5);
    showEntireScene();

    updateGL();
}

void SimpleMeshViewer::drawMesh(bool withName) {
    Mesh::ConstFaceIter    fIt(m_mesh->faces_begin()),
                           fEnd(m_mesh->faces_end());

    Mesh::ConstFaceVertexIter fvIt;
    for (; fIt!=fEnd; ++fIt) {
        OpenMesh::VPropHandleT<unsigned int> groupId;
        unsigned int gId;
        if (!m_mesh->get_property_handle(groupId, "groupId")) {
            gId = 0;
        } else {
            gId = m_mesh->property(groupId, m_mesh->cfv_iter(fIt).handle());
        }

        int id = m_viewMode != COMPONENT ? fIt->idx() : gId;
        if (withName) {
            glPushName(id);
        }

        glBegin(GL_POLYGON);

        if (!withName && selectedName() == id) {
            glColor4f(1.0, 0.0, 0.0, 0.1);
        } else {
            std::vector<float> color = getFaceColor(fIt.handle());
            glColor4f(color[0], color[1], color[2], 0.8);
        }

        fvIt = m_mesh->cfv_iter(fIt.handle());
        glNormal3fv(&m_mesh->normal(fvIt)[0]);
        glVertex3fv(&m_mesh->point(fvIt)[0]);
        ++fvIt;
        glNormal3fv(&m_mesh->normal(fvIt)[0]);
        glVertex3fv(&m_mesh->point(fvIt)[0]);
        ++fvIt;
        glNormal3fv(&m_mesh->normal(fvIt)[0]);
        glVertex3fv(&m_mesh->point(fvIt)[0]);
        glEnd();
        if (withName) {
            glPopName();
        }
    }
}

void SimpleMeshViewer::drawPoint() {
    Mesh::ConstVertexIter   vIt(m_mesh->vertices_begin()),
                            vEnd(m_mesh->vertices_end());
    glBegin(GL_POINTS);
    for (; vIt != vEnd; ++vIt) {
        glColor4f(0.7, 0.7, 0.7, 1.0);
        glNormal3fv(&m_mesh->normal(vIt)[0]);
        glVertex3fv(&m_mesh->point(vIt)[0]);
    }
    glEnd();
}

void SimpleMeshViewer::renderOffscreen(unsigned int width, unsigned int height,
                                       unsigned char *data) {
    if (data == NULL) {
        qDebug() << "Output not initialized.";
        return;
    }

    GLuint renderbuffer;
    GLenum status;

    // Set up a FBO with one renderbuffer attachment
    glGenRenderbuffersEXT(1, &renderbuffer);

    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuffer);

    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA, width, height);

    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                 GL_RENDERBUFFER_EXT, renderbuffer);

    status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);

    if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
    {
        // Handle errors
        qDebug() << "Framebuffer is incomplete\n";
    }

    QColor bgColor = backgroundColor();
    QColor transparentBgColor = bgColor;
    transparentBgColor.setAlpha(0);
    setBackgroundColor(transparentBgColor);

    updateGL();

    setBackgroundColor(bgColor);

    glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glEnable(GL_LIGHTING);

    // Make the window the target
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    // Delete the renderbuffer attachment
    glDeleteRenderbuffersEXT(1, &renderbuffer);
}

std::vector<float> SimpleMeshViewer::getFaceColor(Mesh::FaceHandle fh) {
    std::vector<float> color;

    if (m_viewMode == COMPONENT) {
        OpenMesh::VPropHandleT<unsigned int> groupId;
        if (!m_mesh->get_property_handle(groupId, "groupId")) {
            color = std::vector<float>(3, 0.7);
        } else {
            unsigned int id =
                    m_mesh->property(groupId, m_mesh->cfv_iter(fh).handle());
            QColor cc = getComponentColor(id);
            float red = (float) cc.red() / 255.0;
            float green = (float) cc.green() / 255.0;
            float blue = (float) cc.blue() / 255.0;
            color.push_back(red);
            color.push_back(green);
            color.push_back(blue);
        }
    } else {
        color = std::vector<float>(3, 0.7);
    }
    return color;
}
